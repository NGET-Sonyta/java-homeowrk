package com.HRD;

public class HourlyEmployee extends StaffMember {
    private double hoursWorked;
    private double rate;

    public HourlyEmployee() {
    }

    public HourlyEmployee(StaffMember obj,double hoursWorked, double rate) {
        super.name=obj.name;
        super.id=obj.id;
        super.address=obj.address;
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public HourlyEmployee(int id, String name, String address, double hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    //Getter
    public double getHoursWorked() {
        return hoursWorked;
    }
    public double getRate() {
        return rate;
    }

    //Setter
    public void setHoursWorked(double newHoursWorked){ this.hoursWorked = newHoursWorked; }
    public void setRate(double newRate){
        this.rate = newRate;
    }

    @Override
    public double pay() {
       // double payment=
        return (hoursWorked*rate);
    }


    @Override
    public String toString() {

        return " ID: "+super.id+"\n Name: "+super.name.toUpperCase()+"\n Address: "+super.address+"\n Hours Worked:"+ this.hoursWorked+"\n Rate:"+this.rate+ "\n Payment:"+pay();
    }
}
