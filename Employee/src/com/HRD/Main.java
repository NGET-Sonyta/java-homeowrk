package com.HRD;

import java.util.*;


public class Main {
    public static Scanner input = new Scanner(System.in);
    public static int option1, idInput, idInputUpdate, idInputRemove;
    public static String nameInput, addressInput;
    public static double salaryInput, hourInput, rateInput, bonusInput;
    public static ArrayList<StaffMember> ar = new ArrayList<>();
    public static StaffMember myObj;


    // TO DISPLAY ALL EXISTING STAFF
    public static void display() {
        Collections.sort(ar, Comparator.comparing(StaffMember::getName));

        for (int i = 0; i < ar.size(); i++) {
            //CHECK THE CONDITION BY ID IF IT BELONGS TO VOLUNTEER CLASS
            if (ar.get(i).getClass() == Volunteer.class) {
                Volunteer obj=(Volunteer) ar.get(i);
                System.out.println(obj.toString());
                System.out.println("=======================================");

            }
            //CHECK THE CONDITION BY ID IF IT BELONGS TO HOURLY EMPLOYEE CLASS
            else if (ar.get(i).getClass() == HourlyEmployee.class) {
            HourlyEmployee obj=(HourlyEmployee) ar.get(i);
                System.out.println(obj.toString());
                System.out.println("=======================================");
            }
            //CHECK THE CONDITION BY ID IF IT BELONGS TO SALARIED EMPLOYEE CLASS
            else if (ar.get(i).getClass() == SalariedEmployee.class) {
                 SalariedEmployee obj=new SalariedEmployee();
                 obj= (SalariedEmployee) ar.get(i);
                System.out.println(obj.toString());
                System.out.println("=======================================");
            }
        }
    }

    //FUNCTION PRESS ENTER TO CONTINUE
    public static void pressEnter() {
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }


    // STAFF INFORMATION UPDATE: ID, NAME, ADDRESS (will be used this function in all classes)
    public static void staffInfo() {
        myObj = new StaffMember() {
            @Override
            public double pay() {
                return 0;
            }
        };
        idInput = inputOpt(new Scanner(System.in), "\n ==> Enter Staff Member's ID: ");
        myObj.setId(idInput);
        for (int i = 0; i < ar.size(); i++) {
            if (ar.get(i).getId() == idInput) {
                System.out.println(" \n ~~~~~~~~~ Sorry! The ID cannot be duplicated ~~~~~~~~~ \n");
                staffInfo();
                return;
            }
        }



        System.out.print("\n ==> Enter Staff Member's Name: ");
        nameInput = input.nextLine();
        myObj.setName(nameInput);
        System.out.print("\n ==> Enter Staff Member's Address: ");
        String addressInputHour = input.nextLine();
        myObj.setAddress(addressInputHour);

    }


    //TO UPDATE STAFF INFO BY ID INPUT
    public static void staffUpdate() {
        if(ar.size()==0) {
            System.out.println(" \n ~~~~~~~~~ Sorry! There is 0 staff to update. ~~~~~~~~~ \n");
            option();
        }
        idInputUpdate = inputOpt(new Scanner(System.in), "\n ==> Enter Staff Member's ID to UPDATE: ");
        StaffMember obj;
             for (int i = 0; i < ar.size(); i++) {

                if (ar.get(i).getId() == idInputUpdate) {

                    //CHECK THE CONDITION BY ID IF IT BELONGS TO VOLUNTEER CLASS

                    if (ar.get(i).getClass() == Volunteer.class) {
                        obj = new Volunteer();
                        System.out.println("\n ID: " + ar.get(i).getId() + "\n Name: " + ar.get(i).getName() + "\n Address: " + ar.get(i).getAddress()+"\n Payment: " +ar.get(i).pay());
                        obj.setId(ar.get(i).getId());
                        pressEnter();
                        System.out.println("\n=================== NEW INFORMATION OF STAFF MEMBER ===================\n");
                        System.out.print("\n ==> Enter Staff Member's Name to UPDATE: ");
                        nameInput = input.nextLine();
                        obj.setName(nameInput);
                        System.out.print("\n ==> Enter Staff Member's Address to UPDATE: ");
                        String addressInputHour = input.nextLine();
                        obj.setAddress(addressInputHour);
                        ar.set(i, obj);
                    }
                    //CHECK THE CONDITION BY ID IF IT BELONGS TO HOURLY EMPLOYEE CLASS


                        else if (ar.get(i).getClass() == HourlyEmployee.class) {
                        obj = new HourlyEmployee();
                        System.out.println("\n ID: " + ar.get(i).getId() + "\n Name: " + ar.get(i).getName() + "\n Address: " + ar.get(i).getAddress() + "\n HoursWorked: " + ((HourlyEmployee) ar.get(i)).getHoursWorked() + "\n Rate: " + ((HourlyEmployee) ar.get(i)).getRate()+"\n Payment: " +ar.get(i).pay());
                        pressEnter();

                        System.out.println("\n=================== NEW INFORMATION OF STAFF MEMBER ===================\n");
                        System.out.print("\n ==> Enter Staff Member's Name to UPDATE: ");

                        nameInput = input.nextLine();
                        System.out.print("\n ==> Enter Staff Member's Address to UPDATE: ");
                        String addressInputHour = input.nextLine();
                        Double hourupdate = inputDouble(new Scanner(System.in), "\n ==> Enter Hours: ");
                        Double rateUp = inputDouble(new Scanner(System.in), "\n ==> Enter rates: ");


                        StaffMember myObj1 = new HourlyEmployee(ar.get(i).getId(), nameInput, addressInputHour, hourupdate, rateUp);
                        ar.set(i, myObj1);
                    }
                    //CHECK THE CONDITION BY ID IF IT BELONGS TO SALARIED EMPLOYEE CLASS

                    else if (ar.get(i).getClass() == SalariedEmployee.class) {
                        obj = new SalariedEmployee();
                        System.out.println("\n ID: " + ar.get(i).getId() + "\n Name: " + ar.get(i).getName() + "\n Address: " + ar.get(i).getAddress() + "\n Salary: " + ((SalariedEmployee) ar.get(i)).getSalary() + "\n Bonus: " + ((SalariedEmployee) ar.get(i)).getBonus()+"\n Payment: " +ar.get(i).pay());
                        pressEnter();
                        System.out.println("\n=================== NEW INFORMATION OF STAFF MEMBER ===================\n");
                        System.out.print("\n ==> Enter Staff Member's Name to UPDATE: ");

                        String nameUp = input.nextLine();
                        System.out.print("\n ==> Enter Staff Member's Address to UPDATE: ");
                        String addressInputHour = input.nextLine();
                        Double hourupdate = inputDouble(new Scanner(System.in), "\n ==> Enter Hours: ");
                        Double rateUp = inputDouble(new Scanner(System.in), "\n ==> Enter Rate: ");


                        StaffMember myObj1 = new SalariedEmployee(ar.get(i).getId(), nameUp, addressInputHour, hourupdate, rateUp);
                        ar.set(i, myObj1);
                    } else {
                        System.out.println("~~~~~~~~~ Sorry! The ID cannot be found. ~~~~~~~~~");
                    }
                    System.out.println("\n................... Updated Successfully ...................\n");
                }

        }
        pressEnter();
        display();
        option();
    }


    public static void staffDelete() {
        System.out.println("\n=================== DELETE ===================\n");
        if (ar.size() == 0) {
            System.out.println(" \n ~~~~~~~~~ Sorry! There is 0 staff to remove :( ~~~~~~~~~ \n");
            pressEnter();
            option();
        }
        idInputRemove = inputOpt(new Scanner(System.in), "\n ==> Enter Staff Member's ID to REMOVE: ");
        int b = 0;
        for (int i = 0; i < ar.size(); i++) {
            if (ar.get(i).getId() == idInputRemove) {
                b = 1;
                //CHECK THE CONDITION IF IT BELONGS TO VOLUNTEER CLASS
                if (ar.get(i).getClass() == Volunteer.class) {
                    System.out.println(ar.get(i).toString());
                    ar.remove(i);

                    //CHECK THE CONDITION IF IT BELONGS TO HOURLY EMPLOYEE CLASS
                } else if (ar.get(i).getClass() == HourlyEmployee.class) {
                    System.out.println(ar.get(i).toString());
                    ar.remove(i);
                    //CHECK THE CONDITION IF IT BELONGS TO SALARIED EMPLOYEE CLASS
                } else if (ar.get(i).getClass() == SalariedEmployee.class) {
                    System.out.println(ar.get(i).toString());
                    ar.remove(i);
                }
                pressEnter();
                System.out.println("\n~~~~~~~~ Successfully Removed ~~~~~~ \n");
            }
        }
        if (b == 0) {
            System.out.println("~~~~~~~ Sorry! The ID doesn't match. ~~~~~~~~~");
            staffDelete();
        }
        pressEnter();
        display();
        option();

    }

    //VALIDATION FUNCTION INPUT BY INTEGER
    public static Integer inputOpt(Scanner sc, String st) {
        System.out.print(st);

        if (sc.hasNextInt()) {
            int number = sc.nextInt();
            return number;
        } else {
            System.out.print("\n ~~~~~~~~~ Invalid Input! Only Numbers are Allowed. ~~~~~~~~~ \n");
            return inputOpt(new Scanner(System.in), st);
        }

    }

    //VALIDATION FUNCTION INPUT BY DOUBLE
    public static Double inputDouble(Scanner sc, String st) {
        System.out.print(st);

        if (sc.hasNextDouble()) {
            double doubleNum = sc.nextDouble();
            return doubleNum;
        } else {
            System.out.print("\n ~~~~~~~~~ Invalid Input! Alphabets are NOT Allowed. ~~~~~~~~~ \n");
            return inputDouble(new Scanner(System.in), st);
        }

    }

    //TO DISPLAY AND CHOOSE OPTION
    public static void option() {
        System.out.println("\n1). Add Employee      2). Edit     3). Remove     4). Exit \n");
        Integer number = inputOpt(new Scanner(System.in), "\n ==> Please Choose Option (1-4): ");
        // To Check User Input Number with Condition
        if (number > 4 || number <= 0) {
            System.out.println(" ----------- Accept Number between (1-4) Only! ----------- ");
            number = inputOpt(new Scanner(System.in), " ==> Please Choose Option (1-4): ");
        }
        // User Input Number goes into Switch Case
        switch (number) {
            case 1:
                System.out.println("\n    1). Volunteer      2). Hourly Emp     3). Salary Emp     4). Back \n");

                option1 = inputOpt(new Scanner(System.in), "\n ==> Please Choose Option (1-4): ");
                if (option1 > 4 || number <= 0) {
                    System.out.println(" ----------- Accept Number between (1-4) Only! ----------- ");
                    option1 = inputOpt(new Scanner(System.in), " ==> Please Choose Option (1-4): ");
                }
                switch (option1) {
                    case 1:
                        volunteerStaff();
                        break;
                    case 2:
                        hourlyEmp();
                        break;
                    case 3:
                        salaryEmp();
                        break;
                    case 4:
                        option();
                    default:
                        option();
                }
                break;
            case 2:
                staffUpdate();
                break;
            case 3:
                staffDelete();
                break;
            // User Input Number goes to default when it doesn't match any case number
            default:
                System.out.println("\n >>>>>>>>>>>> GOOD BYE <<<<<<<<<<<<<\n");
                System.exit(0);
        }

    }

    // FUNCTION ADD VOLUNTEER STAFF
    public static void volunteerStaff() {

        myObj = new Volunteer(idInput, nameInput,addressInput);
        System.out.print("\n============= INSERT INFO =============\n");
        staffInfo();
        Volunteer myobj1=new Volunteer(myObj.id,myObj.name,myObj.address);
        ar.add(myobj1);
        System.out.println(myobj1.toString());
        System.out.println("\n................... Added Successfully ...................\n");
        pressEnter();
        display();
        option();

    }

    // FUNCTION ADD HOURLY STAFF
    public static void hourlyEmp() {


        System.out.print("\n============= INSERT INFO =============\n");
        staffInfo();
        hourInput = inputDouble(new Scanner(System.in), "\n ==> Enter Hours: ");
        rateInput = inputDouble(new Scanner(System.in), "\n ==> Enter Rate: ");
        StaffMember myObj1 = new HourlyEmployee(myObj, hourInput, rateInput);
        ar.add(myObj1);
        System.out.println(myObj1.toString());
        System.out.println("\n................... Added Successfully ...................\n");
        pressEnter();
        display();



        option();
    }

    // FUNCTION ADD VOLUNTEER STAFF
    public static void salaryEmp() {

        System.out.println("\n============= INSERT INFO =============\n");
        staffInfo();
        salaryInput = inputDouble(new Scanner(System.in), "\n ==> Enter Salary: ");

        bonusInput = inputDouble(new Scanner(System.in), "\n ==> Enter Bonus: ");
        StaffMember myObj1 = new SalariedEmployee(myObj, salaryInput, bonusInput);
        ar.add(myObj1);
        System.out.println(myObj1.toString());
        System.out.println("\n................... Added Successfully ...................\n");
        pressEnter();
        display();
        option();

    }

    public static void array() {
        System.out.println("\n>>>>>>>>>>>>>>> Initialized Staff <<<<<<<<<<<<<<<<\n");

        ar.add(new SalariedEmployee(1, "Julie", "#12F st322 Brooklyn", 342.9, 89.09));
        ar.add(new Volunteer(2, "Katerine", "#83E st09 Melbourne"));
        ar.add(new HourlyEmployee(3, "Clover", "#63N st78 Wellington", 2.2, 3.4));

        Collections.sort(ar, Comparator.comparing(StaffMember::getName));
        ar.forEach(item->{
                System.out.println("=======================================");
                System.out.println(item.toString());

        });
        option();
        
    }

    static class Sortbyname implements Comparator<StaffMember> {

        //USED FOR SORTING AN ASCENDING ORDER OF STAFF NAME
        ArrayList<StaffMember> arrSort = new ArrayList<>();

        public int compare(StaffMember a, StaffMember b) {
            return a.name.compareTo(b.name);
        }
    }

    public static void main(String[] args) {

        array();
    }
}
