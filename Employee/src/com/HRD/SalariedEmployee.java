package com.HRD;

public class SalariedEmployee extends StaffMember {

    private double salary;
    private double bonus;


    public SalariedEmployee(StaffMember obj,double salary, double bonus) {
        super.name=obj.name;
        super.id=obj.id;
        super.address=obj.address;
        this.salary = salary;
        this.bonus = bonus;
    }

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public double pay() {
        return salary+bonus;
    }

    @Override
    public String toString() {

        return " ID: "+this.id+"\n Name: "+this.name.toUpperCase()+"\n Address: "+this.address+" \n Salary: "+this.salary+" \n Bonus: "+this.bonus + "\n Payment: "+pay();
    }
    public SalariedEmployee() {
    }

    //Getter
    public double getSalary() {
        return salary;
    }

    public double getBonus() {
        return bonus;
    }

    //Setter
    public void setBonus(double newBonus){ this.bonus = newBonus; }
    public void setSalary(double newSalary){
        this.salary = newSalary;
    }

}
