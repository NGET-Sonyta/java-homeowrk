package com.HRD;

public abstract class StaffMember {
    protected int id;
   // protected int category;
    protected String name;
    protected String address;



    public StaffMember(){};
    public StaffMember(int id, String name, String address) {
        this.id=id;
        this.name=name;
        this.address=address;
        //this.category=category;
    }

    public int getId(){return id;}
    public String getName(){return name;}
    public String getAddress(){return address;}

    // Category: 1=Staff, 2=Hourly, 3=Volunteer



    public void setId(int newId){
        this.id=newId;
    }
    public void setName(String newName){
        this.name=newName;
    }
    public void setAddress(String newAddress){
        this.address=newAddress;
    }

    @Override
    public String toString() {
        return " ID: "+this.id+"\n Name: "+this.name.toUpperCase()+"\n Address: "+this.address+ "\n........Thank You!.......";

    }

    ;
    // return string nei object neng

    public abstract double pay();


}
//Generate Setter and Getter tam Field

