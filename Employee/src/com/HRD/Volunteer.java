package com.HRD;

public class Volunteer extends StaffMember{
    public Volunteer() {
    }

    @Override
    public String toString() {
        return " ID: "+super.id+"\n Name: "+super.name.toUpperCase()+"\n Address: "+super.address+ "\n........Thank You!.......";
    }

    @Override
    public double pay() {
        return 0;
    }

    public Volunteer(int id, String name, String address) {

        super(id, name, address);
    }

}